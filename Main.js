
//load express module
var express = require("express");
//create an instance of Express application
var app = express();

//process command line arguments
// -p port
// id dir
var port = 3000;
var staticDir= __dirname + "/public";
var version = 1;
var i = 2;
while (i < process.argv.length){
    switch (process.argv[i]){
        case "-v":
            console.info("version: %d", version);
            break;
        case "-p":
            i = i + 1;
            port = process.argv[i];
            break;
        case "-d":
            i = i + 1;
            staticDir = process.argv[i];
            break;

        default:
    }
    i += 1;
}


//use public directory for static files
//__dirname provides you the starting position that code starts referencing from i.e. the directory that contains the
// main file
app.use(
    express.static(staticDir)
);

console.info("__dirname: %s", __dirname);

for (var i = 0; i < process.argv.length; i++)
    console.info("argv: %d = %s", i, process.argv[i]);

//set a port
app.set("port",
    process.argv[2] || 3000);

//start the server on port
app.listen(
    app.get("port"),
    //put this to let us know that app has started
    function() {
        console.info("%s started on port %d", process.argv[1], app.get("port"));
        console.info("\tstatic directory: %s", staticDir);
    }
);